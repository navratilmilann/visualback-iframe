// https://github.com/chimurai/http-proxy-middleware
// https://github.com/webpack/webpack-dev-server/issues/
// https://github.com/ember-cli/broccoli-asset-rewrite
// https://github.com/msridhar/rewriting-proxy

import $ from 'jquery';

document.domain = window.Visualback.host_domain;

let message = JSON.stringify({
    loaded: false
});

window.parent.postMessage(message, '*');

$(document).ready(function () {

    let html = $('html'),
        body = $('body'),
        oldXHROpen = window.XMLHttpRequest.prototype.open;

    window.addEventListener('load',function(){
        message = JSON.stringify({
            loaded: true
        });

        window.parent.postMessage(message, '*');

        return false;
    })

    window.addEventListener('click', function (event) {   // stop all click or redirects in comment mode

        let commenting_mode = !$(event.target).hasClass('pin') && $('html.visualback-proxy').length

        if (commenting_mode) {

            event.stopImmediatePropagation();
            event.preventDefault();

            const message = JSON.stringify({
                clientX: event.clientX,
                clientY: event.clientY,
                pageX: event.pageX,
                pageY: event.pageY,
                offsetX: event.offsetX,
                offsetY: event.offsetY,
            });

            window.parent.postMessage(message, '*');

            return false;
        }

    }, true);

    body.on('keypress submit', '*', function (event) {

        if ($('body').hasClass('commenting')) {

            event.stopImmediatePropagation();
            event.preventDefault();
        }
    });

    body.find("*").hover(function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        if (html.hasClass('visualback-proxy')) {

            body.find('.on-hover').removeClass('on-hover');
            $(event.target).addClass("on-hover");
        }

    }, function (event) {

        event.preventDefault();
        event.stopImmediatePropagation();

        if (html.hasClass('visualback-proxy')) {

            body.find('.on-hover').removeClass('on-hover');
            $(event.target).addClass("on-hover");
        }
    });

    window.XMLHttpRequest.prototype.open = function (method, url, async, user, password) {
        let is_external_source;

        // console.log(url)

        if (!is_absolute_path(url)) {
            // if (url.charAt(0) === '/') {
            //     url = url.slice(1);
            // }
            url = window.Visualback.redirect_url + url;
        }

        // console.log(url)
        is_external_source = new URL(url).origin !== location.origin;

        if (!is_external_source) {
            arguments[1] = url;
        }

        return oldXHROpen.apply(this, arguments);
    }

    function is_absolute_path(url) {
        return (url.indexOf('://') > 0 || url.indexOf('//') === 0);
    }

});
