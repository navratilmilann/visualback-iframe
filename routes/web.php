<?php

/** @var \Laravel\Lumen\Routing\Router $router */

// https://www.sitepoint.com/community/t/find-urls-in-a-string-with-preg-match-all/6826/2

use App\Services\MakeRequest;

$callback = function (\Illuminate\Http\Request $request) {

    $identifier      = get_subdomain();
    $site            = \App\Models\Site::where('identifier', $identifier)->first();
    $parseSite       = new MakeRequest($site, $request);
    $data            = $parseSite->do();

    return response($data['content'], 200)
        ->header('Content-Type', $data['mime_type']);
};

// todo: sudo python ~/dnsproxy.py -s 8.8.8.8
// https://serverfault.com/questions/1000416/how-do-i-do-a-wildcard-subdomain-on-macos-10-15

$router->post('/_subdomains/{slug}/_url[/{params:.*}]', function (\Illuminate\Http\Request $request) {

    $site      = \App\Models\Site::where('slug', $request->slug)->first();
    $parseSite = new MakeRequest($site, $request);
    $data      = $parseSite->do();

    return response($data['content'], 200)
        ->header('Content-Type', $data['mime_type']);
});

$router->group(['domain' => '{account}.devchicks.test'], function () use ($router, $callback) {

//    $router->get('test', [
//        function (\Illuminate\Http\Request $request) {
//        dd(tidy_get_release());
//            return phpinfo();
//        }
//    ]);

    $router->get('[{params:.*}]', [
        'as' => 'site.livePreview.url',
        $callback
    ]);
    $router->post('[{params:.*}]', $callback);
    $router->put('[{params:.*}]', $callback);
    $router->patch('[{params:.*}]', $callback);
    $router->delete('[{params:.*}]', $callback);
    $router->options('[{params:.*}]', $callback);
});
