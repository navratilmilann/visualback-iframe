<?php

namespace App\Services;

use App\Models\Site;

class ReplaceIMG {

    private $dom, $domain, $site, $app_url;

    /**
     * ReplaceCSS constructor.
     * @param Site $site
     * @param string $domain
     * @param \DOMDocument $dom
     */
    public function __construct(Site $site, string $domain, \DOMDocument $dom) {
        $this->dom     = $dom;
        $this->site    = $site;
        $this->domain  = $domain;
        $this->app_url = self::getAppUrl();
    }

    /**
     * @return string
     */
    private function getAppUrl() {

        $id = $this->site->identifier;

        return sprintf(env('APP_URL'), $id);
    }

    /**
     * @return \DOMNodeList
     */
    private function getAssets() {
        return $this->dom->getElementsByTagName('img');
    }

    /**
     * @param $url
     * @return array
     */
    private function parsedUrl(string $url): array {
        return parse_url($url);
    }

    /**
     * @param string $removedPrefixUrl
     * @return string
     */
    private function getVisualbackAbsolutePath(string $removedPrefixUrl): string {
//        return $this->app_url . '_subdomains/' . $this->site->slug . '/_url/' . $removedPrefixUrl;
        return $this->app_url . $removedPrefixUrl;
    }

    /**
     * @param array $parsedUrl
     * @param string $url
     * @return string
     */
    private function getOriginalAbsolutePath(array $parsedUrl, string $url): string {

        return !empty($parsedUrl['host'])
            ? $url
            : $this->site->base_url . $url;
    }

    /**
     * @param string $originalUrl
     * @return string
     */
    private function getCorrectUri(string $originalUrl): string {

        $uri = $originalUrl;

        if ($originalUrl[0] !== '/') $uri = '/' . $originalUrl;

        return $uri;
    }

    /**
     * @param string $originalUrl
     * @return string
     */
    private function downloadAsset(string $originalUrl): string {

        if (!empty($originalUrl)) {

            $parsedUrl            = self::parsedUrl($originalUrl);
            $originalAbsolutePath = self::getOriginalAbsolutePath($parsedUrl, $originalUrl);

            return self::getVisualbackAbsolutePath($originalAbsolutePath);
        }

        return $originalUrl;
    }

    /**
     * @param $url
     * @return bool
     */
    private function isExternal($url): bool {

        $components = parse_url($url);

        return !empty($components['host']) && strcasecmp($components['host'], $this->domain); // empty host will indicate url like '/relative.php'
    }

    /**
     * @param $asset
     */
    private function setAsset($asset): void {

        $originalUrl = $asset->getAttribute('src');

        if ($originalUrl) {
            $asset = get_asset_attribute($asset, $originalUrl, $this->site, $this->app_url, 'src');

            $asset->removeAttribute('integrity');
        }
    }

    private function rewriteAssets(): void {

        $assets = self::getAssets();

        foreach ($assets as $index => $cssFile) {

            $asset = $assets->item($index);

            self::setAsset($asset);
        }
    }

    /**
     * @return \DOMDocument
     */
    public function do(): \DOMDocument {

        self::rewriteAssets();

        return $this->dom;
    }
}
