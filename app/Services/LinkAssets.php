<?php

namespace App\Services;

use App\Models\Site;
use PHPHtmlParser\Dom;

class LinkAssets {

    private $dom, $domain, $redirect_base_url, $uri, $site, $app_url, $site_url;

    // todo: fixnut fonty
    // todo: dupik.sk - CONSOLE --> http://localhost:9999/?wc-ajax=get_refreshed_fragments

    /**
     * DownloadAsset constructor.
     * @param Site $site
     * @param string $domain
     * @param string $uri
     */
    public function __construct(Site $site, string $domain, string $uri, $site_url) {
        $this->site              = $site;
        $this->domain            = $domain;
        $this->uri               = self::getUri($uri);
        $this->redirect_base_url = self::getRedirectBaseUrl();
        $this->app_url           = self::getAppUrl();
        $this->site_url          = $site_url;

//        $this->
// = new \DOMDocument('1.0', 'utf-8');
    }

    /**
     * @return string
     */
    private function getAppUrl() {

        $id = $this->site->identifier;

        return sprintf(env('APP_URL'), $id);
    }

    /**
     * @param string $uri
     * @return string
     */
    private function getUri(string $uri): string {
        return $uri != '' ? $uri : '/';
    }

    /**
     * @param $content
     * @param $app_url
     * @return string|string[]
     */
    private function addCustomJSTag($content, $app_url) {

        $base_element = '';

        if (strpos($content, '<base')) {
            $content = preg_replace('/(<base[ ]*?href=").+(")/', "$1$app_url$2", $content);
        }
        else {
            $base_element = "<base href='" . $this->site_url . "/' target='_self'>";
        }

        $html = "
        <head>
        <link rel='stylesheet' href='" . $this->app_url . env('IFRAME_CSS_URL') . "'>
        <script type='text/javascript'>" . 'window.Visualback = {"uri":"' . $this->uri . '","base_url":"' . $this->site->url . '", "redirect_url": "' . $this->app_url . '", "host_domain":"' . env('HOST_DOMAIN') . '"};' . "</script>
        <script type='text/javascript' src='" . $this->app_url . env('IFRAME_JS_URL') . "'></script>
        " .
            $base_element . "
        ";

        return str_replace('<head>', $html, $content);
    }

    /**
     * @return string
     */
    private function getRedirectBaseUrl(): string {
        return route('site.livePreview.url', ['slug' => $this->site->slug]);
    }

    /**
     * @return array[]
     */
    private function getLinks(): array {
        return [
            'form'   => [
                'items'     => $this->dom->getElementsByTagName('form'),
                'attribute' => 'action',
            ],
            'anchor' => [
                'items'     => $this->dom->getElementsByTagName('a'),
                'attribute' => 'href',
            ],
            'base'   => [
                'items'     => $this->dom->getElementsByTagName('base'),
                'attribute' => 'href',
            ],
        ];
    }

    /**
     * @param $url
     * @return bool
     */
    private function isExternal($url): bool {

        $components = parse_url($url);

        return !empty($components['host']) && strcasecmp($components['host'], $this->domain); // empty host will indicate url like '/relative.php'
    }

    /**
     * @param string $content
     * @return \tidy
     */
    private function repairHTML(string $content): \tidy {

//        $tidy   = new \tidy();
//        $config = [
//            'clean'                       => 'yes',
//            'output-html'                 => 'yes',
//            'drop-empty-elements'         => 'no',
//            'new-blocklevel-tags'         => 'p nav article aside audio bdi canvas details dialog figcaption figure footer header hgroup main menu menuitem nav section source summary template track video',
//            'new-inline-tags'             => 'audio command datalist embed keygen mark menuitem meter output progress source time video wbr i',
//            'new-empty-tags'              => 'i',
//        ];
//
//        $tidy->parseString($content, $config, 'UTF8');
//
//        $tidy->cleanRepair();

        $tidy   = new \tidy();
        $config = [
            'clean'               => 'yes',
            'output-html'         => 'yes',
            'drop-empty-elements' => 'no',
//            'new-blocklevel-tags' => 'p nav article aside audio bdi canvas details dialog figcaption figure footer header hgroup main menu menuitem nav section source summary template track video',
//            'new-inline-tags'     => 'audio command datalist embed keygen mark menuitem meter output progress source time video wbr i',
        ];

        $tidy->parseString($content, $config, 'UTF8');
        $tidy->cleanRepair();

        return $tidy;
    }

    /**
     * @param \DOMElement $asset
     * @param array $assetType
     * @param string $tag
     */
    private function setLinkUrl(\DOMElement $asset, array $assetType, string $tag): void {

        $originalUrl = $asset->getAttribute($assetType['attribute']);

        if ($originalUrl) {
            $asset = get_asset_attribute($asset, $originalUrl, $this->site, $this->app_url, $assetType['attribute']);

            if ($asset->getAttribute('target') == '_blank') {
                $asset->removeAttribute('target');
            }
        }
    }

    private function rewriteLinkUrl(): void {

        $links = self::getLinks();

        foreach ($links as $tag => $linkType) {

            foreach ($linkType['items'] as $index => $link) {

                $link = $linkType['items']->item($index);

                self::setLinkUrl($link, $linkType, $tag);
            }
        }
    }

    /**
     * @param string $content
     * @return string|string[]
     */
    public function do(string $content) {

//        @$this->dom->loadHTML(self::repairHTML($content));
//
////        libxml_use_internal_errors(true);
//
//        $this->dom = (new ReplaceIMG($this->site, $this->domain, $this->dom))->do();
//
//        $this->dom = (new ReplaceJS($this->site, $this->domain, $this->dom))->do();
//
//        $this->dom = (new ReplaceCSS($this->site, $this->domain, $this->dom))->do();
//
//        self::rewriteLinkUrl();
//
        $app_url = self::getAppUrl();

        $content = self::addCustomJSTag($content, $app_url);

        return str_replace($this->site->base_url, $app_url, $content);
    }
}
