<?php

namespace App\Services;

use App\Models\Site;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Http\Request;
use Mockery\Exception;
use PHPHtmlParser\Dom;

class MakeRequest {

    // todo: limitacia velkosti webu, napr. nepovoliť stiahnutie youtubu
    // todo: vyskusat rozne typy requestov
    // todo: vyskusat react/vuejs appky
    // todo: rozdelit js fily, aby nebol jeden taky velky

    private $uri, $domain, $directory_path, $download_asset, $site_url, $site, $request, $app_url, $redirect_base_uri, $data;

    /**
     * MakeRequest constructor.
     * @param Site $site
     * @param Request $request
     */
    public function __construct($site, $request) {
        $this->app_url           = env('APP_STORAGE_PATH');
        $this->site              = $site;
        $this->request           = $request;
        $this->site_url          = self::getSiteUrl();
        $this->redirect_base_uri = self::getRedirectBaseUri();
        $this->domain            = self::getDomain();
        $this->uri               = self::getRequestUri();
        $this->directory_path    = self::getDirectoryPath();

        $this->download_asset = new LinkAssets(
            $this->site,
            $this->domain,
            $this->uri,
            $this->site_url
        );
    }

    /**
     * @param $url
     * @return string|string[]
     */
    private function replaceSingleSlash(string $url): string {

        if (!str_contains($url, '://'))
            return str_replace(':/', '://', $url);

        return $url;
    }

    /**
     * @return string
     */
    private function getRedirectBaseUri(): string {
        return route('site.livePreview.url', ['slug' => $this->site->slug, 'params' => $this->site->base_url]);
    }

    private function getSiteUrl(): string {

//        $uri = str_replace($this->site->base_url, '', $this->request->params);
//        $url = $this->site->base_url .'/' .$uri;


//        if (!empty($_SERVER['HTTP_REFERER'])) {
//            return $_SERVER['HTTP_REFERER']. $_SERVER['REQUEST_URI'];
//        } else {
            return $this->site->base_url . $_SERVER['REQUEST_URI'];
//        }
    }

    /**
     * @return string
     */
    private function getRequestUri(): string {
        return parse_url($this->site_url)['path'] ?? '';
    }

    /**
     * @return string
     */
    private function getDirectoryPath(): string {
        return $this->app_url . '/' . $this->domain;
    }

    /**
     * @return string
     */
    private function getDomain(): string {
        return parse_url($this->site_url)['host'];
    }

    /**
     * @return CookieJar
     */
    private function getCookieJar(): CookieJar {
        return CookieJar::fromArray(
            $this->request->cookies->all()
            , $this->domain);
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function makeRequestWithoutData(): \Psr\Http\Message\ResponseInterface {

        $client    = new Client();
        $cookieJar = self::getCookieJar();

//        $headers   = self::getHeaders();

        return $client->request($this->request->getMethod(), $this->site_url, [
//                'headers'         => $headers,
                'cookies'         => $cookieJar,
                'allow_redirects' => true,
                'verify'          => false
            ]
        );
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function makeRequestWithData(): \Psr\Http\Message\ResponseInterface {

        $client    = new Client(['cookies' => true]);
        $cookieJar = self::getCookieJar();

//        $headers   = self::getHeaders();

        return $client->request($this->request->getMethod(), $this->site_url, [
//            'headers'         => $headers,
            'cookies'         => $cookieJar,
            'form_params'     => $this->request->all(),
            'allow_redirects' => true,
            'verify'          => false
        ]);
    }

    /**
     * @return array
     */
    private function getHeaders(): array {

        $headers = getallheaders();

        if ($this->request->getMethod() == 'POST')
            $headers['Content-type'] = 'multipart/form-data';

//        $headers['Referer'] = $this->site->base_url;

//        unset($headers['Cookie']);
        unset($headers['Host']);

        return $headers;
    }

    private function setCookies() {

        $cookie = new Cookie();

        foreach ($this->data['cookies'] as $index => $cookieString) {

            $parsedCookie = $cookie->parseCookies($cookieString);
            $httpOnly     = strpos($parsedCookie->path, 'httponly') ? true : false;

            $data = [
                'expires'  => $parsedCookie->expires,
                'path'     => '/',
                'domain'   => '',
                'httponly' => $httpOnly,
            ];

            setcookie($parsedCookie->name, $parsedCookie->value, $data);
            $_COOKIE[$parsedCookie->name] = $parsedCookie->value;
        }
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getSiteByUrl(): void {

        if ($this->request->isMethod('get') || $this->request->isMethod('head')) {

            $response = self::makeRequestWithoutData();

            $this->data = [
                'content'   => $response->getBody(),
                'cookies'   => $response->getHeaders()['Set-Cookie'] ?? [],
                'mime_type' => $response->getHeader('Content-type')[0] ?? '',
            ];

        } else {

            $response = self::makeRequestWithData();

            $this->data = [
                'content'   => $response->getBody(),
                'cookies'   => $response->getHeaders()['Set-Cookie'] ?? [],
                'mime_type' => $response->getHeader('Content-type')[0],
            ];
        }
    }

    /**
     * @return string
     */
    private function rewriteHTML(): string {

        return $this->download_asset
            ->do($this->data['content']);
    }

    private function onlyHTML(): void {

        if (str_contains($this->data['mime_type'], 'text/html')) {

            self::setCookies();

            $this->data['content'] = self::rewriteHTML();
        }
    }

    /**
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function do(): array {

        try {

            self::getSiteByUrl();

            self::onlyHTML();

            return $this->data;

        } catch (\GuzzleHttp\Exception\ClientException $exception) {

            return [
                'content'   => $exception->getResponse()->getBody()->getContents(),
                'mime_type' => 'text/html'
            ];
        }
    }
}
