<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Site extends Model {
    use HasFactory;

    protected $fillable = [
        'name', 'url', 'base_url', 'slug', 'description', 'rating', 'evaluable', 'active', 'user_id'
    ];

    protected $casts    = [
        'created_at' => 'datetime:d.m.Y H:i',
    ];
}
